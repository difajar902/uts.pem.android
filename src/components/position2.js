import React from 'react';
import {Text,Image, View, ScrollView, StyleSheet} from 'react-native';
import Position2 from '../assets/dini.png';

const User = () => {
    return (
        <View>
            <ScrollView>
                <View style={styles.first}>
                    <View style={styles.icon}>
                        <Image source={Position2} style={styles.image}/> 
                    </View>
                    <Text style={styles.tanda}>+</Text>
                    <View>
                    <Text style={styles.text}>Cerita Anda</Text>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create ({
    first: {
        marginTop: 0,
        marginLeft: 10,
    },
    icon: {
        borderWidth: 1,
        borderColor: 'white',
        width: 65,
        height: 65,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 65
    },
    image: {
        width: 60,
        height: 60,
        borderRadius: 100
    },
    tanda: {
        fontSize: 14,
        color: 'white',
        backgroundColor: 'skyblue',
        padding : 2,
        width: 24,
        height: 24,
        borderRadius: 80,
        position: 'absolute',
        textAlign: 'center',
        margin: 40,
        marginRight: 2

    },
    text: {
        fontSize: 13,
        margin: 5,
        marginLeft: 1,
        marginTop: 4
    }
})

export default User;