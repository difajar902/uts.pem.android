import React from 'react';
import {View, Text, Image, StyleSheet, ScrollView} from 'react-native';
import Message from './src/components/position';
import User from './src/components/position2';
import Friend from './src/components/position21';
import Friend2 from './src/components/position22';
import Body from './src/components/conten1';
import Body2 from './src/components/conten2';
import Body3 from './src/components/conten3';
import Body4 from './src/components/conten4';
import Body5 from './src/components/conten5';
import Body6 from './src/components/conten6';
import Body7 from './src/components/conten7';
import Body8 from './src/components/conten8';
import Body9 from './src/components/conten9';

const App = () => {
    return (
      <View style={styles.utama}>
            <View>
                <View style={styles.box}>
                    <Text style={styles.text}>Instagram</Text>
                <View>
                    <Image style={styles.love} source={require('./src/assets/love1.jpg')}/>
                </View>
                <Message />
                </View>
            </View> 
        <ScrollView>         
            <View style={styles.imp}>
              <User />
              <Friend />
              <Friend2 />
            </View>
            <View>
                <Body />
                <Body2 />
                <Body3 />
                <Body4 />
                <Body5 />
                <Body6 />
                <Body7 />
                <Body8 />
                <Body9 />
            </View>
        </ScrollView>
      </View>

    );
};
const styles = StyleSheet.create({
  utama: {
    backgroundColor: 'white'
  },
  box: {
    padding : 5,
    flexDirection: 'row',
    backgroundColor:'white'
  },
  text: {
    margin: 10,
    fontFamily: 'times new roman',
    fontSize: 25,
    fontWeight: 'bold'
  },
  love: {
    borderRadius: 100,
    marginTop: 3,
    width: 50,
    height: 50,
    marginLeft: 100,
  },
  imp: {
    flexDirection: 'row',
  },
  box1: {
    padding : 2,
    flexDirection: 'row',
    backgroundColor:'white',
    marginLeft: 0,
  },
  
    
})

export default App;